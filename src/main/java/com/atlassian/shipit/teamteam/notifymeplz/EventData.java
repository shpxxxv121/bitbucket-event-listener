package com.atlassian.shipit.teamteam.notifymeplz;

import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.user.ApplicationUser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/*
{
    "actor": {
        "name": "Ze'ev",
        "email": "zgilovitz@atlassian.com",
        "avatar": "maybe" ???
    },
    "verb": "Commented on",
    "target": {
        "title": "SHPXXXV121 - Atlassian Event Feed",
        "link": "http://extranet.atlassian.com/......"
    }
    "recipient": "pthompson@atlassian.com",

    // A product specific rendering allowing a more details to be displayed
    // This would be sweet, but lets ingnore it for now
    "details": {
        "description": "Expand to view the comment",
        "html": "<p>Yo! This is a comment </p>"
    }
}
 */
class EventData {
    private Actor actor;
    private String action;
    private Target target;
    private String recipient;
    private Map<String, String> details = new HashMap<>();
    private long time;

    private static class Actor {
        String name;
        String email;
        String avatar;
    }

    private static class Target {
        String title;
        String link;
    }

    public static class Builder {
        private final EventData instance;

        Builder() {
            this.instance = new EventData();
            addDetail("time", String.valueOf(new Date().getTime()));
        }

        public Builder actor(String name, String email, String avatar) {
            Actor actor = new Actor();
            actor.name = name;
            actor.email = email;
            actor.avatar = avatar;
            instance.actor = actor;
            return this;
        }

        public Builder action(String action) {
            instance.action = action;
            return this;
        }

        public Builder actor(ApplicationUser user, AvatarService avatarService) {
            return actor(user.getDisplayName(), user.getEmailAddress(), avatarService.getUrlForPerson(user, new AvatarRequest(false, 48)));
        }

        public Builder target(String title, String link) {
            Target target = new Target();
            target.title = title;
            target.link = link;
            instance.target = target;
            return this;
        }

        public Builder recipient(String recipient) {
            instance.recipient = recipient;
            return this;
        }

        public Builder addDetail(String key, String value) {
            instance.details.put(key, value);
            return this;
        }


        public EventData build() {
            return instance;
        }

    }

    public static Builder builder() {
        return new Builder();
    }
}
