package com.atlassian.shipit.teamteam.notifymeplz;

import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.event.project.ProjectCreatedEvent;
import com.atlassian.bitbucket.event.pull.*;
import com.atlassian.bitbucket.event.repository.RepositoryCreatedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryRefsChangedEvent;
import com.atlassian.bitbucket.pull.PullRequestParticipant;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class EventHandler {

    private Gson gson = new Gson();
    private String urlString = "https://shpxxxv121-web-backend.internal.domain.dev.atlassian.io/api/v1.0/events";
//    private String urlString = "http://ac023aeb.ngrok.io/api/v1.0/events";
    private AvatarService userAvatarService;

    @Autowired
    public EventHandler(@ComponentImport AvatarService userAvatarService) {
        this.userAvatarService = userAvatarService;
    }

    @EventListener
    public void onCreateRepo(RepositoryCreatedEvent event) throws Exception {
        ApplicationUser user = event.getUser();
        EventData eventData = EventData.builder()
                .action("bitbucket-server.repository-created")
                .actor(user.getDisplayName(),
                        user.getEmailAddress(),
                        userAvatarService.getUrlForPerson(user, new AvatarRequest(false, 24)))
                .target(event.getRepository().getName(),
                        event.getRepository().getSlug())
                .recipient("*")
                .addDetail("projectName", event.getRepository().getProject().getName())
                .addDetail("isPublic", "" + event.getRepository().getProject().isPublic())
                .build();
        sendData(eventData);
    }

    @EventListener
    public void onPRCommentedAddedEvent(PullRequestCommentEvent event) throws Exception {
        Set<PullRequestParticipant> reviewers = event.getPullRequest().getReviewers()
                .stream()
                .filter(x -> !event.getUser().equals(x))
                .collect(Collectors.toSet());
        reviewers.add(event.getPullRequest().getAuthor());
        for (PullRequestParticipant reviewer : reviewers) {
             EventData eventData = EventData.builder()
                    .action("bitbucket-server.pullrequest.comment.added")
                    .actor(event.getUser(), userAvatarService)
                    .recipient(reviewer.getUser().getEmailAddress())
                    .target(event.getPullRequest().getTitle(), "" + event.getPullRequest().getId())
                    .build();
            sendData(eventData);
        }
    }

    @EventListener
    public void onPRMergedEvent(PullRequestMergedEvent event) throws Exception {
        Set<PullRequestParticipant> reviewers = event.getPullRequest().getReviewers();
        for (PullRequestParticipant reviewer : reviewers) {
            EventData eventData = EventData.builder()
                    .action("bitbucket-server.pullrequest.merged")
                    .actor(event.getUser(), userAvatarService)
                    .recipient(reviewer.getUser().getEmailAddress())
                    .target(event.getPullRequest().getTitle(), "" + event.getPullRequest().getId())
                    .build();
            sendData(eventData);
        }
    }

    @EventListener
    public void onPRReviewerAddedEvent(PullRequestParticipantsUpdatedEvent event) throws Exception {
        sendAddNotification(event.getAddedParticipants(), event);
        sendRemoveNotification(event.getRemovedParticipants(), event);

        if(event.getUser().getId() != event.getPullRequest().getAuthor().getUser().getId()) {
//             Then the PR has been updated by someone who isn't the author
            EventData eventData = EventData.builder()
                    .action("bitbucket-server.pullrequest.external.updated")
                    .actor(event.getUser(), userAvatarService)
                    .target(event.getPullRequest().getTitle(), "" + event.getPullRequest().getId())
                    .recipient(event.getPullRequest().getAuthor().getUser().getEmailAddress())
                    .build();
            sendData(eventData);
        }
    }

    @EventListener
    public void onPRUpdatedEvent(PullRequestUpdatedEvent event) throws Exception {
        Set<PullRequestParticipant> reviewers = event.getPullRequest().getReviewers();
        for (PullRequestParticipant reviewer : reviewers) {
            EventData eventData = EventData.builder()
                    .action("bitbucket-server.pullrequest.updated")
                    .actor(event.getUser(), userAvatarService)
                    .recipient(reviewer.getUser().getEmailAddress())
                    .target(event.getPullRequest().getTitle(), "" + event.getPullRequest().getId())
                    .build();
            sendData(eventData);
        }
    }

    @EventListener
    public void onProjectCreated(ProjectCreatedEvent event)
            throws Exception {
        ApplicationUser user = event.getUser();
        EventData eventData = EventData.builder()
                .action("bitbucket-server.project-created")
                .actor(user.getDisplayName(),
                        user.getEmailAddress(),
                        userAvatarService.getUrlForPerson(user, new AvatarRequest(false, 24)))
                .target(event.getProject().getName(),
                        event.getProject().getKey())
                .build();
        sendData(eventData);
    }

    @EventListener
    public void onRepoModified(RepositoryRefsChangedEvent event) throws Exception {
        ApplicationUser user = event.getUser();
        Repository repository = event.getRepository();
        EventData eventData = EventData.builder()
                .action("bitbucket-server.repository.defaultbranch.modified")
                .actor(user, userAvatarService)
                .target("Repository default branch updated", "asdf")
                .recipient("*")
                .addDetail("repoName", repository.getName())
                .addDetail("projectName", repository.getProject().getName())
                .addDetail("repoAvatar", userAvatarService.getUrlForProject(repository.getProject(), new AvatarRequest(false, 48)))
                .build();
        sendData(eventData);
    }

    private void sendAddNotification(Set<ApplicationUser> users, PullRequestParticipantsUpdatedEvent event) throws Exception {
        for (ApplicationUser removedParticipant : users) {
            EventData eventData = EventData.builder()
                    .action("bitbucket-server.pullrequest.user.added")
                    .actor(event.getUser(), userAvatarService)
                    .recipient(removedParticipant.getEmailAddress())
                    .target(event.getPullRequest().getTitle(), "" + event.getPullRequest().getId())
                    .build();
            sendData(eventData);
        }
    }

    private void sendData(EventData event) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");

        String input = gson.toJson(event);

        OutputStream os = conn.getOutputStream();
        os.write(input.getBytes());
        os.flush();

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }

        conn.disconnect();
    }

    private void sendRemoveNotification(Set<ApplicationUser> removedParticipants, PullRequestParticipantsUpdatedEvent event) throws Exception {
        for (ApplicationUser removedParticipant : removedParticipants) {
            EventData eventData = EventData.builder()
                    .action("bitbucket-server.pullrequest.user.removed")
                    .actor(event.getUser(), userAvatarService)
                    .recipient(removedParticipant.getEmailAddress())
                    .target(event.getPullRequest().getTitle(), "" + event.getPullRequest().getId())
                    .build();
            sendData(eventData);
        }
    }
}
